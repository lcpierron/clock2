# Projet : clock-display

Auteurs: Michael Kölling and David J. Barnes
Traduction: Laurent Pierron

Ce projet fait partie du matériel pour le livre :

   **Objects First with Java - A Practical Introduction using BlueJ
   Fifth edition
   David J. Barnes and Michael Kölling
   Pearson Education, 2012**

Il est expliqué dans le chapitre 3.

Une réalisation de l'affichage d'une horloge numérique; illustre les concepts d'abstraction, découpage en module, et d'interaction avec des objets. Une version avec une interface graphique animée (GUI) est incluse dans la branche `clock-gui`.

---

## TÂCHE 1 : Préparation du projet

1. Créer une bifurcation (fork) du projet **clock-display** dans votre compte Bitbucket.
2. Ajouter votre binôme et `lcpierron`en administrateur du projet.
3. Cloner votre projet sur votre machine locale.

---

## L'horloge : un exemple

Le but de ce projet est de programmer l'affichage d'une horloge numérique comme dans les exemples ci-dessous :

![différentes horloges numériques](dclock_images.gif)

Nous allons d'abord nous concentrer sur la mécanique interne de l'horloge et nous réaliserons l'affichage sous forme de texte dans  une fenêtre console.

Tout d'abord nous réaliserons une horloge au format européen avec un affichage sur 24 heures, puis nous la compliquerons légèrement pour un affichage américain sur 12 heures avec indication de la demi-journée.

Dans un prochain cours nous réaliserons une version graphique de cette horloge.

### *Divide et impera*

Afin de maîtriser la complexité d'un logiciel, comme celui de la réalisation d'une voiture ou d'un immeuble, on divisera le projet en sous-projets plus simples les plus indépendants possibles, celà jusqu'à ce que l'on ne sache plus diviser à nouveau.

Cette division se fait au moment de la conception même si au fil du développement on peut constater la nécessité de diviser à nouveau certains problèmes, c'est même assez classique si on utilise une méthode de développement incrémentale et une gestion de projet *agile*. Nous y reviendrons plus tard dans le cours.

Ce projet d'horloge est suffisamment simple pour être contenu dans une seule classe, mais il va nous servir d'exemple pour décomposer un problème.

### Programmation de l'horloge

Le problème de l'horloge peut être divisé en deux sous-projets :

1. Gérer un compteur numérique cyclique ;
1. Gérer une horloge qui utilise deux compteurs cycliques.

Pour chaque sous-projet nous réaliserons une classe Java dont le but sera de :

- créer les valeurs initiales ;
- modifier les valeurs des objets suivant les règles du monde des horloges ;
- afficher les valeurs des objets, c'est l'**expression des besoins**.

Voici l'entête et les attributs de ces deux classes.

  ```java
public class NumberDisplay
{
  private int limit;
  private int value;
  ...
}

public class ClockDisplay
{
  private NumberDisplay hours;
  private NumberDisplay minutes;
  ...
}
  ```

Vous pouvez constater dans la définition de `ClockDisplay` l'utilisation de deux objets de la classe `NumberDisplay`.

### TÂCHE 2. Étude du code source de la classe `ClockDisplay`

1. Ouvrir le projet `clock-display`dans BlueJ, créez un objet `ClockDisplay` sans paramètres et un autre avec paramètres et expérimentez les différentes méthodes.

1. Quelle est la valeur initiale de `ClockDisplay`, quand le constructeur est appelé sans paramètres ?

1. Pourquoi n'y a-t-il pas d'accesseurs (`getHours()`et `getMinutes()` pour les variables `hours` et `minutes`, serait-ce utile ?

1. Pourquoi n'y a-t-il pas de mutateurs (`setHours()`et `setMinutes()` pour les variables `hours` et `minutes`, serait-ce utile ?

1. Quelles sont les méthodes qui modifient l'objet `ClockDisplay`?

1. Pourquoi la méthode `updateDisplay()` n'est pas visible depuis le menu contextuel de l'objet dans BlueJ ? Quel est son intérêt alors ?

1. La classe `ClockDisplay` a deux constructeurs, dans un seul des deux il y a un appel à la méthode `updateDisplay()`, est-ce un oubli dans l'autre ? Pourquoi ? Comment pourrait-t-on faire pour ne plus avoir `updateDisplya()` dans le premier constructeur ?

1. La méthode prédéfinie `this` de Java permet d'appeler un autre constructeur de la même classe dans un constructeur. Pour appeler le second constructeur depuis le premier on peut écrire : `this(10,20)`. Simplifiez au maximum le corps du constructeur sans paramètres en utilisant `this`. Est-ce une bonne manière de procéder ? Pourquoi ?

### TÂCHE 3. Étude du code source de la classe `NumberDisplay`

1. Dans BlueJ depuis un objet `ClockDisplay` consultez le contenu des objets `NumberDisplay`

1. Quelle instruction Java a permis la création des objets `NumberDisplay` ?

1. Dans le schéma **UML** des classes de l'interface BlueJ comment on peut voir la relation de dépendance entre les classes `NumberDisplay` et `ClockDisplay` ?

1. Que se passe-t-il si la méthode `setValue()` est appelée avec une valeur illégale ? Est-ce une bonne solution ? Pouvez-vous trouver une meilleure solution ?

La méthode `setValue()` contient une expression **conditionnelle**, cette dernière permet de choisir les instructions suivantes à exécuter en fonction du résultat de l'expression **booléenne** placée après le mot-clef `if`.

Voici cette instruction conditionnelle :

   ```java
   if((replacementValue >= 0) && (replacementValue < limit)) {
     value = replacementValue;
   }
   ```

   > Les expressions **booléennes**, sont des expressions dont l'évaluation donne une des deux valeurs : **VRAI** ou **FAUX** respectivement `true` et `false` dans le langage de programmation Java et dans beaucoup d'autres langages de programmation.
   >
   > Les valeurs et expressions booléennes peuvent être combinées en Java avec les opérateurs `&&` (ET), `||` (OU) et `!` (NON).

1. Que se passerait-t-il si dans la méthode `setValue()` on remplaçait dans l'instruction conditionnelle `&&` par `||` ? Comment changer l'expression *booléenne* pour remplacer l'opérateur `&&` par l'opératuer `||`.

1. Quelle autre méthode de la classe `NumberDisplay` utilise une expression booléenne ?

1. Dans la méthode `getDisplayValue`, il y a deux instructions `return` ? Modifiez la méthode pour n'avoir plus qu'un seul `return`.

  > Ce n'est pas une bonne pratique de programmation d'avoir plusieurs instructions `return` dans une méthode, car cela rend difficile la compréhension et la modification de la méthode.

1. Est-ce que `getDisplayValue()` fonctionne correctement dans tous les cas ? Quelles sont ses préconditions ? Que se passe-t-il si on donne une limite de 800 à l'objet `NumberDisplay`?

1. Dans la méthode `increment()`, on trouve une expression avec l'opérateur `%` (*modulo*). Que fait cet opérateur ?
1. Remplacez dans la méthode `increment()` l'instruction d'affectation avec l'opérateur *modulo* par une instruction conditionnelle.

### TÂCHE 4 Amélioration de l'horloge

1. Créez une nouvelle branche pour votre développement : `git checkout -b clock12`

1. Transformez l'horloge 24 heures en une horloge 12 heures. Attention l'affichage à midi et minuit se fait commençant par 12, par exemple `12:30` pour midi et demi, donc les heures vont de 1 à 12 mais pas de 0 à 11.

1. Après avoir testé, enregistrez votre travail : `git commit -m "Horloge 12 heures." .`

1. Maintenant on souhaite encore améliorer l'horloge pour choisir d'afficher, sous la forme 24 heures, sous la forme 12 heures comme la précédente ou sous la forme américaine en indiquant la partie du jour : `4.23pm`. Créez trois méthodes chacune retournant un des formats d'heure suivant les besoins précisés précédemment.

1. Créez encore une nouvelle branche pour un nouveau développement : `git checkout -b clock3form`. Vous pouvez le faire avant d'avoir enregistrez votre travail sous le gestionnaire de version.

1. Après avoir testé, enregistrez votre travail : `git commit -m "Horloge triple format." .`

1. Synchronisez toutes vos branches sur le serveur : `git push --all`

---

## Tester, re-tester, tester encore et toujours

Pour s'assurer qu'un logiciel a le comportement attendu et demandé, il faut tester ce logiciel.

Depuis le début de cours nous avons largement testé les projets exemples en créant des objets et en appelant des méthodes sur ces objets pour voir comment le logiciel fonctionnait, mais aussi pour voir si le comportement était correct lorsque nous modifions le logiciel.

Mais tester un logiciel en créant des objets via une interface graphique, c'est long et laborieux et puis vous risquez d'oublier de tester certaines fonctionnalités. Une solution consiste à avoir un protocole de test écrit sur une feuille, mais pourquoi ne pas automatiser ce protocole autant que possible, c'est ce que vous allez commencer à voir.

### TÂCHE 5 Tests automatisés de la classe ClockDisplay

1. Retour sur la branche principale du projet : `git checkout master`

1. Créez une nouvelle branche pour ajouter les tests : `git checkout -b testing`

1. Lancer BlueJ, ouvrez le projet `clock-display`

1. Dans le menu contextuel de la classe `ClockDisplay` sélectionnez `Créez classe Test`, une classe de test appelez `ClockDisplayTest` est créée, elle contiendra les méthodes de test de la classe `ClockDisplay`.

#### Premier test

Vous allez créer un premier test permettant de vérifier que la valeur affichée par défaut de la classe `ClockDisplay`est `"00:00"`

1. Dans le menu contextuel de `ClockDisplay` sélectionnez `Enregistrez une méthode de test`, nommez là `defaultClockShouldBeMidnight`, vous passez en mode **Enregistrement** signalé par un voyant rouge sur la gauche de la fenêtre et deux boutons activés.

1. Créez un objet `ClockDisplay` et appelez-le `defaut`.

1. Dans le menu contextuel de `defaut` appelez la méthode `getTime()`, une boîte de dialogue augmentée s'affiche avec une zone **Assertion** permettant de donner la valeur attendue en retour de l'appel à la méthode, dans cette valeur tapez `"00:00"`.

1. Cliquez sur le bouton `Terminer` en dessous du voyant rouge. Votre premier test est enregistré et compilé.

1. Cliquez sur `Exécuter les tests`, une nouvelle fenêtre apparaît signalant la liste des test effectués et leur bon déroulement.

1. Enregistrez tout cela sur le gestionnaire de version : `git add ClockDisplayTest.java; git commit -m "Premier test de ClockDisplay." ClockDisplayTest.java`

#### Autres tests de `ClockDisplay`

Créez les tests suivants pour vérifier le bon fonctionnement des objets `ClockDisplay`, le nom de chaque méthode test devrait refléter la fonction tester et la valeur attendue, après chaque nouvelle méthode de test pensez à enregistrer votre travail avec la commande `git commit`:

1. Ajout de 1 tick à l'horloge par défaut doit donner la valeur `"00:01"`, exemple de nom de méthode de test: `settingOneTickToDefaultShouldDisplay00H01M`

1. Positionnement d'une heure prédéfinie par la mèthode `setTime`, exemple de méthode de test : `settingTime10H37MToDefaultShouldDisplay10H37M`. Dans la méthode de test de `setTime`, vous pouvez tester plusieurs affectation d'heure, par exemple le matin, l'après-midi, avant 10h00, etc. Testez quatre heures différentes et changez le nom de la méthode.

1. Testez que l'ajout de 1 tick à une horloge prédéfinie à la valeur `"23:59"` affiche `"00:00"`

1. Testez la création d'un objet `ClockDisplay(heures, minutes)`

1. Vérifiez que la création d'un objet `ClockDisplay(hours,minutes)` affiche la même chose que la création d'un objet `ClockDisplay()` suivi de l'appel à la méthode `setTime(hours, minutes)`

#### Tests de `NumberDisplay`

Réalisez une suite de tests pour la classe `NumberDisplay`, on testera :

1. la création d'objet ;
1. les deux méthodes *mutator* ;
1. et surtout le passage à zéro à la valeur limite.

---
